package jp.alhinc.mori_hiroko.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class CalculateSales {

	public static void main(String[] args) {
        Map<String,String> names = new TreeMap<>();//全体で使うなら一番↑で宣言
        Map<String,Long> totals = new TreeMap<>();

		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			if(!file.exists()){
				System.out.println("支店定義ファイルが存在しません。");
				return;
            }
            try {
                br = new BufferedReader(new FileReader(file));//FileReader fr = new FileReader(file);
                String line;								   //↑つづき br = new BufferedReader(fr);	の略
                while((line = br.readLine()) != null){
                    String[] items = line.split(",");
                    if(items.length != 2 || items[0].matches("^[0-9]{3}") == false){
                        System.out.println("支店定義ファイルのフォーマットが不正です");
                        return;
                    }
                    //先にエラーを出させてから正常処理を入れたほうが変な処理入れないですむ？
                    names.put(items[0], items[1]);
                    totals.put(items[0], 0L);
                }
            } finally {
                br.close();
            }

			FilenameFilter filter = new FilenameFilter(){
				public boolean accept(File file, String str){
					return new File(file, str).isFile() && str.matches("[0-9]{8}.rcd");
				}
            };

            File[] rcdFiles = new File(args[0]).listFiles(filter);
            Arrays.sort(rcdFiles);//昇順にする

			if(rcdFiles != null) {
                int min = Integer.parseInt(rcdFiles[0].getName().substring(0, 8));//ファイル名の最小値取得
                int max = Integer.parseInt(rcdFiles[rcdFiles.length - 1].getName().substring(0, 8));//ファイル一覧の要素数から1引いた数の
                                                                                                    インデックスのファイル名
                if (min + rcdFiles.length - 1 != max) {//ファイル名の最小値と一覧全要素数を足して-1　これがmaxにならなければエラー
                    System.out.println("売上ファイル名が連番になっていません");//例）1,2,3のファイルある
                    return;													//	1+（3）-1=3	true
                }								//例）2,3,5のファイルある→2+(3)-1=4 5にならなきゃいけない	false

				BufferedReader br2 = null;
				try {
                    for(File rcdFile : rcdFiles) {
                        br2 = new BufferedReader(new FileReader(rcdFile));
                        String code = br2.readLine();
                        String sales = br2.readLine();
                        if(br2.readLine() != null){//既に2行呼んでいるからnullじゃなきゃおかしい
                            System.out.println(rcdFile.getName() + "のフォーマットが不正です");
                            return;
                        }

                        if(names.containsKey(code) == false){
                            System.out.println(rcdFile.getName() + "の支店コードが不正です");
                            return;
                        }

                        Long total = totals.get(code) + Long.parseLong(sales);//0に金額足していくよ
                        if(total.toString().length() > 10){//文字列にして桁数で判断、100000～と入れる際は_で桁区切りすると見やすい
                            System.out.println("合計金額が10桁を超えました");
                            return;
                        }

                        totals.put(code, total);//金額をmapに入れるよ
					}
				}
				finally{
					br2.close();
				}
            }

            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(args[0], "branch.out"))));//1度宣言して終わるやつは略しちゃう

            for (String code : names.keySet()) {
                pw.println(code + "," + names.get(code) + "," + totals.get(code));
            }
			pw.close();
		}
		catch(Exception e){//大tryにその他のエラーはお任せ
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}

